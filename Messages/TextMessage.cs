﻿using System;

namespace Messages
{
    public class TextMessage
    {
        public string Text { get; set; }
        public byte[] data { get; set; }
    }
}
