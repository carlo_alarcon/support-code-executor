﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Messages
{
    public class PackageData
    {
        public string Id { get; set; }
        public string Language { get; set; }
        public byte[] Data { get; set; }
    }
}
