﻿using EasyNetQ;
using EasyNetQ.Topology;
using Messages;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PackageListener
{
    class Program
    {
        static AutoResetEvent autoEvent = new AutoResetEvent(false);
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            PackageData p = new PackageData();
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                var advancedBus = bus.Advanced;
                var queueInstance = advancedBus.QueueDeclare("PackageQueue");
                var exchangeInstance = advancedBus.ExchangeDeclare("PackageExchange", ExchangeType.Direct);
                advancedBus.Bind(exchangeInstance, queueInstance, "PackageListenerMessage");
                //advancedBus.Consume(queueInstance, (body, properties, info) => Task.Factory.StartNew(() =>{
                //    var message = Encoding.UTF8.GetString(body);
                //    PackageData p = JsonConvert.DeserializeObject<PackageData>(message);
                //    File.WriteAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\resources1\\resources1.zip", p.Data);
                //    Console.WriteLine(message);
                //}));
                advancedBus.Consume(queueInstance, HandleExecution);
                //                autoEvent.WaitOne();
            }
        }
        private static Task HandleExecution(byte[] body, MessageProperties properties, MessageReceivedInfo info)
        {
            return Task.Factory.StartNew(() =>
            {
                var message = Encoding.UTF8.GetString(body);
                PackageData p = JsonConvert.DeserializeObject<PackageData>(message);
                File.WriteAllBytesAsync("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\resources1\\resources1.zip", p.Data);
                //_codeExecutionService.Execute(codeInformation);
                // CodeInformation1 codeInformation = JsonConvert.DeserializeObject<CodeInformation1>(message);
                // _codeExecutionService.ExecuteProgram(codeInformation);
            });
        }

    }
}