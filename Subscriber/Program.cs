﻿using System;
using System.IO;
using EasyNetQ;
using Messages;

namespace Subscriber
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                bus.PubSub.Subscribe<TextMessage>("test", HandleTextMessage);
                Console.WriteLine("Listenning for messages. Hit <return> to quit.");
                Console.ReadLine();
            }
        }
        static void HandleTextMessage(TextMessage textMessage)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Got message: {0}", textMessage.Text);
            Console.ResetColor();
            File.WriteAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\resources1\\resources1.zip", textMessage.data);
        }
    }
}
