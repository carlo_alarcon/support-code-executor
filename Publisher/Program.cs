﻿using EasyNetQ;
using Messages;
using System;
using System.IO;

namespace Publisher
{
    class Program
    {
        static void Main(string[] args)
        {
//            File.WriteAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\resources1\\temp.txt", data);
//            byte[] newData = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\resources1\\temp.txt");
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                var input = String.Empty;
                Console.WriteLine("Enter a message. 'Quit' to quit.");

                while ((input = Console.ReadLine()) != "Quit")
                {
                    byte[] data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\resources\\resources.zip");
                    bus.PubSub.Publish(new TextMessage { Text = input, data = data });
                    Console.WriteLine("Message published!");
                }
            }
        }
    }
}
