﻿using EasyNetQ;
using EasyNetQ.Topology;
using Messages;
using System;
using System.IO;

namespace PackagePublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                var advancedBus = bus.Advanced;
                var queue = advancedBus.QueueDeclare("PackagingQueue");
                var exchange = advancedBus.ExchangeDeclare("PackagingExchange", ExchangeType.Direct);
                advancedBus.Bind(exchange, queue, "PackagingMessage");
                byte[] data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\python\\source.zip");
                PackageData p = new PackageData { Id = "1", Language = "python", Data = data };
                advancedBus.Publish(exchange, "PackagingMessage", true, new Message<PackageData>(p));
                for (int i = 2; i < 10; i++)
                {
                    p = new PackageData { Id = i.ToString(), Language = "python", Data = data };
                    advancedBus.Publish(exchange, "PackagingMessage", true, new Message<PackageData>(p));
                }
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\cpp\\source.zip");
                for (int i = 10; i < 20; i++)
                {
                    p = new PackageData { Id = i.ToString(), Language = "cpp", Data = data };
                    advancedBus.Publish(exchange, "PackagingMessage", true, new Message<PackageData>(p));
                }
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\python\\source.zip");
                for (int i = 20; i < 31; i++)
                {
                    p = new PackageData { Id = i.ToString(), Language = "python", Data = data };
                    advancedBus.Publish(exchange, "PackagingMessage", true, new Message<PackageData>(p));
                }
            }
        }
    }
}
