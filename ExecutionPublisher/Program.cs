﻿using EasyNetQ;
using EasyNetQ.Topology;
using Messages;
using System;
using System.IO;

namespace ExecutionPublisher
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var bus = RabbitHutch.CreateBus("host=localhost"))
            {
                var advancedBus = bus.Advanced;
                var queue = advancedBus.QueueDeclare("ExecutionQueue");
                var exchange = advancedBus.ExchangeDeclare("ExecutionExchange", ExchangeType.Direct);
                advancedBus.Bind(exchange, queue, "ExecutionMessage");
                byte[] data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\python\\source.zip");
                PackageData p = new PackageData { Id = "40", Language = "python", Data = data };
                advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\cpp\\source.zip");
                p = new PackageData { Id = "41", Language = "cpp", Data = data };
                advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\ruby\\source.zip");
                p = new PackageData { Id = "42", Language = "ruby", Data = data };
                advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\js\\source.zip");
                p = new PackageData { Id = "43", Language = "javascript", Data = data };
                advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\python\\source.zip");
                p = new PackageData { Id = "44", Language = "python", Data = data };
                advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\java\\source.zip");
                p = new PackageData { Id = "45", Language = "java", Data = data };
                advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\python\\source.zip");
                for (int i = 46; i < 50; i++)
                {
                    p = new PackageData { Id = i.ToString(), Language = "python", Data = data };
                    advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                }
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\ruby\\source.zip");
                for (int i = 50; i < 60; i++)
                {
                    p = new PackageData { Id = i.ToString(), Language = "ruby", Data = data };
                    advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                }
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\java\\source.zip");
                for (int i = 60; i < 70; i++)
                {
                    p = new PackageData { Id = i.ToString(), Language = "java", Data = data };
                    advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                }
                data = File.ReadAllBytes("D:\\jalasoft-BC-carlo\\project-services\\EasyNetQTest\\js\\source.zip");
                for (int i = 70; i < 80; i++)
                {
                    p = new PackageData { Id = i.ToString(), Language = "javascript", Data = data };
                    advancedBus.Publish(exchange, "ExecutionMessage", true, new Message<PackageData>(p));
                }
            }
        }
    }
}
