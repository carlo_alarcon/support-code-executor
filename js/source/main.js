var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
  terminal: false
});

let input = []
rl.on('line', function(line){
	input.push(line)
}).on('close', function() {
	let a = Number(input.shift())
	let b = Number(input.shift())
	console.log((a+b))
})